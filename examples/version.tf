terraform {
  required_version = ">= 0.13.6"
  required_providers {
    aws = "~> 3.39.0"
  }
}