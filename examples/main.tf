resource "aws_api_gateway_account" "module" {
  cloudwatch_role_arn = aws_iam_role.cloudwatch.arn
}

module "rest-api" {
  source                       = "../api/."
  name                         = "rest"
  description                  = "rest-api"
  api_key_source               = "HEADER"
  disable_execute_api_endpoint = false
  client_certificate           = false
  request_validator            = true
  authorizer = [{
    name                             = "authorizer1"
    authorizer                       = "authorizer"
    authorizer_uri                   = "authorizer_uri"
    authorizer_arn                   = "authorizer_arn"
    type                             = "TOKEN"
    identity_source                  = "identity_source"
    authorizer_result_ttl_in_seconds = "300"
    identity_validation_expression   = ""
    provider_arns                    = ""
  }]
  policy = {
    policy_doc = ""
  }
  models = [{
    name         = "model"
    description  = "model"
    content_type = "content_type"
    model_schema = "model_schema"
  }]
  gateway_responses = [{
    status_code = "200"
    type        = "application/json"
    template    = {
      "app" = "json"
    }
    parameters  = {
      "app" = "json"
    }
  }]
  tags = {}
}