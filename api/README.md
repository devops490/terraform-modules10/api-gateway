## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.15.3 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 3.39.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 3.39.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_api_gateway_authorizer.module](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_authorizer) | resource |
| [aws_api_gateway_client_certificate.module](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_client_certificate) | resource |
| [aws_api_gateway_gateway_response.module](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_gateway_response) | resource |
| [aws_api_gateway_model.module](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_model) | resource |
| [aws_api_gateway_request_validator.module](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_request_validator) | resource |
| [aws_api_gateway_rest_api.module](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_rest_api) | resource |
| [aws_api_gateway_rest_api_policy.module](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_rest_api_policy) | resource |
| [aws_iam_role.module](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy.module](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_api_key_source"></a> [api\_key\_source](#input\_api\_key\_source) | The source of the API key for requests. | `string` | `"HEADER"` | no |
| <a name="input_authorizer"></a> [authorizer](#input\_authorizer) | The properties for the authorizer | `list(map)` | `[]` | no |
| <a name="input_binary_media_types"></a> [binary\_media\_types](#input\_binary\_media\_types) | The list of binary media types supported by the RestApi. By default, the RestApi supports only UTF-8-encoded text payloads. | `list(string)` | <pre>[<br>  "UTF-8-encoded"<br>]</pre> | no |
| <a name="input_client_certificate"></a> [client\_certificate](#input\_client\_certificate) | Boolean to create client certificate | `bool` | `false` | no |
| <a name="input_description"></a> [description](#input\_description) | The description for the API Gateway | `string` | `""` | no |
| <a name="input_disable_execute_api_endpoint"></a> [disable\_execute\_api\_endpoint](#input\_disable\_execute\_api\_endpoint) | Specifies whether clients can invoke your API by using the default execute-api endpoint. | `bool` | `false` | no |
| <a name="input_gateway_responses"></a> [gateway\_responses](#input\_gateway\_responses) | THe list of gateway responses for the API | `list(map)` | `[]` | no |
| <a name="input_minimum_compression_size"></a> [minimum\_compression\_size](#input\_minimum\_compression\_size) | Minimum response size to compress for the REST API. Integer between -1 and 10485760 (10MB). Setting a value greater than -1 will enable compression, -1 disables compression (default). | `number` | `-1` | no |
| <a name="input_models"></a> [models](#input\_models) | THe list of models for the API | `list(map)` | `[]` | no |
| <a name="input_name"></a> [name](#input\_name) | The name of the REST API | `string` | n/a | yes |
| <a name="input_policy"></a> [policy](#input\_policy) | The policy block to contain policy document | `map(any)` | `{}` | no |
| <a name="input_request_validator"></a> [request\_validator](#input\_request\_validator) | Boolean to create request validator | `string` | `""` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Key-value map of resource tags. | `map(string)` | `{}` | no |
| <a name="input_types"></a> [types](#input\_types) | A list of endpoint types | `string` | `"EDGE"` | no |
| <a name="input_vpc_endpoint_ids"></a> [vpc\_endpoint\_ids](#input\_vpc\_endpoint\_ids) | Set of VPC Endpoint identifiers. | `list(string)` | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_api_arn"></a> [api\_arn](#output\_api\_arn) | The API Gateway API ARN |
| <a name="output_api_id"></a> [api\_id](#output\_api\_id) | The API Gateway API ID |
| <a name="output_authorizer"></a> [authorizer](#output\_authorizer) | The list of authorizers |
| <a name="output_certificate_arn"></a> [certificate\_arn](#output\_certificate\_arn) | The ID of the certificate |
| <a name="output_certificate_id"></a> [certificate\_id](#output\_certificate\_id) | The ID of the certificate |
| <a name="output_certificate_public_key"></a> [certificate\_public\_key](#output\_certificate\_public\_key) | The PEM-encoded public key of the client certificate. |
| <a name="output_execution_arn"></a> [execution\_arn](#output\_execution\_arn) | The execution ARN of API |
| <a name="output_model_id"></a> [model\_id](#output\_model\_id) | The ID of the API model |
| <a name="output_policy_id"></a> [policy\_id](#output\_policy\_id) | The policy for the API Gateway |
| <a name="output_request_validator"></a> [request\_validator](#output\_request\_validator) | The request validator |
| <a name="output_root_resource_id"></a> [root\_resource\_id](#output\_root\_resource\_id) | The root resource ID |
