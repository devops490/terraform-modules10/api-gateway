# aws_api_gateway_rest_api

variable "name" {
  type        = string
  description = "The name of the REST API"
}

variable "description" {
  type        = string
  default     = ""
  description = "The description for the API Gateway"
}

variable "types" {
  type        = string
  default     = "EDGE"
  description = "A list of endpoint types"
  validation {
    condition     = contains(["EDGE", "REGIONAL", "PRIVATE"], var.types)
    error_message = "Invalid endpoint type."
  }
}

variable "vpc_endpoint_ids" {
  type        = list(string)
  default     = []
  description = "Set of VPC Endpoint identifiers."
}

variable "binary_media_types" {
  type        = list(string)
  default     = ["UTF-8-encoded"]
  description = "The list of binary media types supported by the RestApi. By default, the RestApi supports only UTF-8-encoded text payloads."
}

variable "minimum_compression_size" {
  type        = number
  default     = -1
  description = "Minimum response size to compress for the REST API. Integer between -1 and 10485760 (10MB). Setting a value greater than -1 will enable compression, -1 disables compression (default)."
}


variable "api_key_source" {
  type        = string
  default     = "HEADER"
  description = "The source of the API key for requests."
  validation {
    condition     = contains(["HEADER", "AUTHORIZER"], var.api_key_source)
    error_message = "Invalid API key source."
  }
}

variable "disable_execute_api_endpoint" {
  type        = bool
  default     = false
  description = "Specifies whether clients can invoke your API by using the default execute-api endpoint."
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Key-value map of resource tags."
}

# aws_api_gateway_rest_api_policy

variable "policy" {
  type        = map(any)
  default     = {}
  description = "The policy block to contain policy document"
}

# aws_api_gateway_authorizer

variable "authorizer" {
  type        = list(map(any))
  default     = [{}]
  description = "The properties for the authorizer"
}

# aws_api_gateway_request_validator

variable "request_validator" {
  type        = string
  default     = ""
  description = "Boolean to create request validator"
}


# aws_api_gateway_client_certificate

variable "client_certificate" {
  type        = bool
  default     = false
  description = "Boolean to create client certificate"
}

# aws_api_gateway_model

variable "models" {
  type        = list(map(any))
  default     = []
  description = "THe list of models for the API"
}

# aws_api_gateway_gateway_response

variable "gateway_responses" {
  type = list(object({
    status_code = string
    type        = string
    template    = map(string)
    parameters  = map(string)
  }))
  default     = []
  description = "THe list of gateway responses for the API"
}