resource "aws_iam_role" "module" {
  name = "${var.name}-role"
  path = "/"

  assume_role_policy = <<-EOF
    {
    "Version": "2012-10-17",
    "Statement": [
        {
        "Action": "sts:AssumeRole",
        "Principal": {
            "Service": "apigateway.amazonaws.com"
        },
        "Effect": "Allow",
        "Sid": ""
        }
    ]
    }
    EOF
}

resource "random_uuid" "module" {}

data "aws_iam_policy_document" "module" {
  count = length(var.authorizer)
  dynamic "statement" {
    for_each = var.authorizer
    content {
      actions   = ["lambda:InvokeFunction"]
      resources = [lookup(statement.value, "authorizer_arn", "*")]
    }
  }
}

resource "aws_iam_role_policy" "module" {
  count  = length(var.authorizer)
  name   = "${var.name}-api-gateway-default-policy-${random_uuid.module.result}"
  role   = aws_iam_role.module.id
  policy = data.aws_iam_policy_document.module[count.index].json
}