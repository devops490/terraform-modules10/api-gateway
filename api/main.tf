resource "aws_api_gateway_rest_api" "module" {
  name        = var.name
  description = var.description
  endpoint_configuration {
    types            = [var.types]
    vpc_endpoint_ids = var.types == "PRIVATE" ? var.vpc_endpoint_ids : null
  }
  binary_media_types           = var.binary_media_types
  minimum_compression_size     = var.minimum_compression_size
  api_key_source               = var.api_key_source
  disable_execute_api_endpoint = var.disable_execute_api_endpoint
  tags                         = var.tags
}

resource "aws_api_gateway_rest_api_policy" "module" {
  count       = length(var.policy)
  rest_api_id = aws_api_gateway_rest_api.module.id
  policy      = var.policy.policy_doc
}

resource "aws_api_gateway_authorizer" "module" {
  for_each                         = { for authorizer in var.authorizer : authorizer.name => authorizer }
  name                             = each.value.name
  rest_api_id                      = aws_api_gateway_rest_api.module.id
  authorizer_uri                   = lookup(each.value, "authorizer", null)
  authorizer_credentials           = aws_iam_role.module.arn
  identity_source                  = lookup(each.value, "identity_source", "method.request.header.Authorization")
  type                             = lookup(each.value, "type", "TOKEN")
  authorizer_result_ttl_in_seconds = lookup(each.value, "authorizer_result_ttl_in_seconds", 300)
  identity_validation_expression   = lookup(each.value, "identity_validation_expression", null)
  provider_arns                    = each.value.type == "COGNITO_USER_POOLS" ? [each.value.provider_arns] : null
}

resource "aws_api_gateway_request_validator" "module" {
  count                       = var.request_validator ? 1 : 0
  name                        = "${var.name}-validator"
  rest_api_id                 = aws_api_gateway_rest_api.module.id
  validate_request_body       = true
  validate_request_parameters = true
}

resource "aws_api_gateway_client_certificate" "module" {
  count       = var.client_certificate ? 1 : 0
  description = "Client certificate for ${var.name}"
  tags        = var.tags
}

resource "aws_api_gateway_model" "module" {
  for_each     = { for models in var.models : models.name => models }
  rest_api_id  = aws_api_gateway_rest_api.module.id
  name         = each.value.name
  description  = each.value.description
  content_type = lookup(each.value, "content_type", "application/json")
  schema       = lookup(each.value, "schema", "")
}

resource "aws_api_gateway_gateway_response" "module" {
  for_each            = { for gateway_responses in var.gateway_responses : gateway_responses.status_code => gateway_responses }
  rest_api_id         = aws_api_gateway_rest_api.module.id
  status_code         = lookup(each.value, "status_code", "")
  response_type       = lookup(each.value, "type", "")
  response_templates  = each.value.template
  response_parameters = each.value.parameters
}