output "api" {
  value       = aws_api_gateway_rest_api.module.*
  description = "The API Gateway API ID"
}

output "policy_id" {
  value       = aws_api_gateway_rest_api_policy.module.*
  description = "The policy for the API Gateway"
}

output "authorizer" {
  value       = aws_api_gateway_authorizer.module.*
  description = "The list of authorizers"
}

output "validator" {
  value       = aws_api_gateway_request_validator.module.*.id
  description = "The request validator"
}

output "certificate" {
  value       = aws_api_gateway_client_certificate.module.*
  description = "The ID of the certificate"
}

output "model" {
  value       = aws_api_gateway_model.module.*
  description = "The ID of the API model"
}